package br.com.avaliacao.tecnica.controller;

import br.com.avaliacao.tecnica.domain.RamoAtividade;
import br.com.avaliacao.tecnica.service.RamoAtividadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/ramo-atividade")
public class RamoAtividadeController {

    @Autowired
    private RamoAtividadeService ramoAtividadeService;

    @GetMapping(value = "/all")
    public ResponseEntity<List<RamoAtividade>> findAll(){
        List<RamoAtividade> ramoAtividades = ramoAtividadeService.findAll();
        return ResponseEntity.ok().body(ramoAtividades);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity create(@Valid @RequestBody RamoAtividade ramoAtividade) {
        return ResponseEntity.status(HttpStatus.CREATED).body(ramoAtividadeService.insert(ramoAtividade));
    }
}
