package br.com.avaliacao.tecnica.controller;

import br.com.avaliacao.tecnica.domain.Concorrente;
import br.com.avaliacao.tecnica.service.ConcorrenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/concorrentes")
public class ConcorrenteController {

    @Autowired
    private ConcorrenteService concorrenteService;


    @GetMapping(value = "/all")
    public ResponseEntity<List<Concorrente>> findAll(){
        List<Concorrente> concorrentes = concorrenteService.findAll();
        return ResponseEntity.ok().body(concorrentes);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity create(@Valid @RequestBody Concorrente concorrente) {
        return ResponseEntity.status(HttpStatus.CREATED).body(concorrenteService.insert(concorrente));
    }

}
