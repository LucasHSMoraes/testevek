package br.com.avaliacao.tecnica.controller;

import br.com.avaliacao.tecnica.domain.Cliente;
import br.com.avaliacao.tecnica.domain.RamoAtividade;
import br.com.avaliacao.tecnica.service.ClienteService;
import br.com.avaliacao.tecnica.service.RamoAtividadeService;
import br.com.avaliacao.tecnica.util.WriteCsvToResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping(value = "{id}")
    public ResponseEntity<Optional<Cliente>> find(@PathVariable Long id) {
        Optional<Cliente> cliente = clienteService.find(id);
        return ResponseEntity.ok().body(cliente);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Cliente>> findAll(){
        List<Cliente> cliente = clienteService.findAll();
        return ResponseEntity.ok().body(cliente);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity create(@Valid @RequestBody Cliente cliente) {
        List<String> erros = cliente.validarTaxasDescontos(cliente);

        if(erros.size() == 0)
            return ResponseEntity.status(HttpStatus.CREATED).body(clienteService.insert(cliente));

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(erros);
    }


    @PutMapping(value = "/update/{id}")
    public Cliente update(@Valid @RequestBody Cliente cliente, @PathVariable Long id){
        clienteService.find(id);
        return clienteService.update(cliente);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        clienteService.find(id);
        clienteService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/proposta-aceita")
    public void findByAceita(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition","attachment; file=proposta-aceita.csv" );
        List<Cliente> clientes = clienteService.findByAceita();

        WriteCsvToResponse.writeDataToCsvUsingStringArray(response.getWriter(), clientes);
    }
}
