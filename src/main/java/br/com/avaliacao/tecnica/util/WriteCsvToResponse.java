package br.com.avaliacao.tecnica.util;

import br.com.avaliacao.tecnica.domain.Cliente;
import com.opencsv.CSVWriter;

import java.io.PrintWriter;
import java.util.List;

public class WriteCsvToResponse {

    public static void writeDataToCsvUsingStringArray(PrintWriter writer, List<Cliente> clientes) {
        String[] CSV_HEADER = { "id", "concorrente", "cnpjCpf", "Telefones", "email","txConcorrenteCred", "txConcorrenteDeb", "descontoCred", "descontoDeb", "txFinalCred", "txFinalDeb", "Data" };
        try (
                CSVWriter csvWriter = new CSVWriter(writer,
                        ';',
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ){
            csvWriter.writeNext(CSV_HEADER);

            for (Cliente cliente : clientes) {
                String[] data = {
                        cliente.getId().toString(),
                        cliente.getConcorrente().getNome(),
                        cliente.getCnpjCpf(),
                        cliente.getTelefones().toString(),
                        cliente.getEmail(),
                        cliente.getConcorrente().getTxCreditoConcorrente().toString(),
                        cliente.getConcorrente().getTxDebitoConcorrente().toString(),
                        cliente.getProposta().getDescontoCredito().toString(),
                        cliente.getProposta().getDescontoDebito().toString(),
                        cliente.txFinalAceitaCredito(),
                        cliente.txFinalAceitaDebito(),
                        cliente.getProposta().getDataDaProposta().toString()
                };

                csvWriter.writeNext(data);
            }
;
        }catch (Exception e) {
            System.out.println("Erro ao gerar CSV");
            e.printStackTrace();
        }
    }
}