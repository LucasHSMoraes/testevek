package br.com.avaliacao.tecnica.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Proposta {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Campo desconto crédito obrigatório")
    private Double descontoCredito;

    @NotNull(message = "Campo desconto débito obrigatório")
    private Double descontoDebito;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm")
    private Date dataDaProposta;

    private Boolean aceita;

    public Proposta(Double descontoCredito, Double descontoDebito, Date dataDaProposta) {
        this.dataDaProposta = dataDaProposta;
        this.descontoCredito = descontoCredito;
        this.descontoDebito = descontoDebito;
        this.aceita = false;
    }
}
