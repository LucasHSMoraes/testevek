package br.com.avaliacao.tecnica.domain;

import lombok.*;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.NumberFormat;
import java.util.*;

import static java.util.Locale.Category.FORMAT;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Campo concorrente obrigatório")
    @OneToOne
    private Concorrente concorrente;

    @NotNull(message = "Campo Ramo de Atividade obrigatório")
    @ManyToOne
    @JoinColumn(name = "RAMO_ATIVIDADE_ID")
    private RamoAtividade ramoAtividade;

    @ElementCollection
    @CollectionTable(name = "TELEFONE")
    @NotNull(message = "Campo telefone obrigatório")
    private Set<String> telefones = new HashSet<>();

    @NotNull(message = "CNPJ ou CPF inválido")
    private String cnpjCpf;

    @Column(unique = true)
    private String email;

    @OneToOne
    @JoinColumn(name = "proposta_id")
    private Proposta proposta;


    public Cliente(Concorrente concorrente, String cnpjCpf, String email, RamoAtividade ramoAtividade,  Proposta proposta) {
        this.concorrente = concorrente;
        this.ramoAtividade = ramoAtividade;
        this.cnpjCpf = cnpjCpf;
        this.email = email;
        this.proposta = proposta;
    }


    public List<String> validarTaxasDescontos(@RequestBody @Valid Cliente cliente) {
        List<String> erros = new ArrayList<>();

        double taxaDebConcorrente = cliente.getConcorrente().getTxDebitoConcorrente();
        double taxaCredConcorrente = cliente.getConcorrente().getTxCreditoConcorrente();

        double taxaFinalDeb = taxaDebConcorrente - cliente.getProposta().getDescontoDebito();
        double taxaFinalCred = taxaCredConcorrente - cliente.getProposta().getDescontoCredito();

        boolean taxaFinalDebValida = taxaFinalDeb > cliente.getRamoAtividade().getTxMinimaDebito();
        boolean taxaFinalCredValida = taxaFinalCred > cliente.getRamoAtividade().getTxMinimaCredito();

        if(!taxaFinalCredValida)
            erros.add(String.format("Desconto inválido: Taxa mínima de crédito válida é: %3.2f", cliente.getRamoAtividade().getTxMinimaCredito()));

        if(!taxaFinalDebValida)
            erros.add(String.format("Desconto inválido: Taxa mínima de débito válida é: %3.2f", cliente.getRamoAtividade().getTxMinimaDebito()));
        return erros;
    }

    public String txFinalAceitaCredito(){
        double txFinal = getConcorrente().getTxCreditoConcorrente() - getProposta().getDescontoCredito();
        return String.format("%3.2f", txFinal);
    }

    public String txFinalAceitaDebito(){
        double txFinal = getConcorrente().getTxDebitoConcorrente() - getProposta().getDescontoDebito();
        return String.format("%3.2f", txFinal);
    }
}
