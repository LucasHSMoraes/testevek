package br.com.avaliacao.tecnica.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Concorrente {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String nome;

    @JsonIgnore
    @OneToMany(mappedBy = "concorrente")
    private List<Cliente> clientes = new ArrayList<>();

    @NotNull(message = "Campo Taxa Debito Concorrente obrigatório")
    private Double txDebitoConcorrente;
    @NotNull(message = "Campo Taxa Crédito Concorrente obrigatório")
    private Double txCreditoConcorrente;


    public Concorrente(String nome, Double txDebitoConcorrente, Double txCreditoConcorrente) {
        this.nome = nome;
        this.txDebitoConcorrente = txDebitoConcorrente;
        this.txCreditoConcorrente = txCreditoConcorrente;
    }


    @Override
    public String toString() {
        return nome;
    }
}
