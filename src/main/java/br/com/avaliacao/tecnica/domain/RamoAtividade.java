package br.com.avaliacao.tecnica.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class RamoAtividade {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String nome;

    @JsonIgnore
    @OneToMany(mappedBy = "ramoAtividade")
    private List<Cliente> clientes = new ArrayList<>();


    private Double txMinimaCredito;
    private Double txMinimaDebito;


    public RamoAtividade(String nome, Double txMinimaCredito, Double txMinimaDebito) {
        this.nome = nome;
        this.txMinimaCredito = txMinimaCredito;
        this.txMinimaDebito = txMinimaDebito;
    }

}
