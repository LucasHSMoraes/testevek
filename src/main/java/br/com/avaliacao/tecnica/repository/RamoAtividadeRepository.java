package br.com.avaliacao.tecnica.repository;

import br.com.avaliacao.tecnica.domain.RamoAtividade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RamoAtividadeRepository extends JpaRepository<RamoAtividade, Long> {
}
