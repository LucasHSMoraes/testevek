package br.com.avaliacao.tecnica.repository;

import br.com.avaliacao.tecnica.domain.Concorrente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConcorrenteRepository extends JpaRepository<Concorrente, Long> {
}
