package br.com.avaliacao.tecnica.repository;

import br.com.avaliacao.tecnica.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClienteRepository  extends JpaRepository<Cliente, Long> {

    @Query(value = "SELECT * FROM Cliente inner join Proposta ON Cliente.id = Proposta.id AND Proposta.aceita = true", nativeQuery = true)
    List<Cliente> findByAceita();

}
