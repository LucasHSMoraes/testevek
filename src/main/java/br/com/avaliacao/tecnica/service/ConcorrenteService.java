package br.com.avaliacao.tecnica.service;

import br.com.avaliacao.tecnica.domain.Concorrente;
import br.com.avaliacao.tecnica.repository.ConcorrenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConcorrenteService {

    @Autowired
    ConcorrenteRepository concorrenteRepository;

    public List<Concorrente> findAll() {
        return concorrenteRepository.findAll();
    }

    public Concorrente insert( Concorrente concorrente) {
        return concorrenteRepository.save(concorrente);
    }
}
