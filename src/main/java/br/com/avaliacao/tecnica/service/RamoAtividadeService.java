package br.com.avaliacao.tecnica.service;

import br.com.avaliacao.tecnica.domain.RamoAtividade;
import br.com.avaliacao.tecnica.repository.RamoAtividadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RamoAtividadeService {

    @Autowired
    private RamoAtividadeRepository ramoAtividadeRepository;

    public List<RamoAtividade> findAll() {
        return ramoAtividadeRepository.findAll();
    }

    public RamoAtividade insert(RamoAtividade ramoAtividade) {
        return ramoAtividadeRepository.save(ramoAtividade);
    }
}
