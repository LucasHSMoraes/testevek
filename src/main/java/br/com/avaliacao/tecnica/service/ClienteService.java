package br.com.avaliacao.tecnica.service;

import br.com.avaliacao.tecnica.domain.Cliente;
import br.com.avaliacao.tecnica.domain.Proposta;
import br.com.avaliacao.tecnica.domain.RamoAtividade;
import br.com.avaliacao.tecnica.repository.ClienteRepository;
import br.com.avaliacao.tecnica.repository.PropostaRepository;
import br.com.avaliacao.tecnica.repository.RamoAtividadeRepository;
import br.com.avaliacao.tecnica.service.execptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Optional<Cliente> find(Long id) {
        Optional<Cliente> cliente = clienteRepository.findById(id);
        return Optional.ofNullable(cliente.orElseThrow(() -> new ObjectNotFoundException("Não existe cliente com esse id: " + id )));
    }

    public List<Cliente> findAll() {
        return clienteRepository.findAll();
    }

    public List<Cliente> findByAceita(){
        return clienteRepository.findByAceita();
    }

    public Cliente insert(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente update(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public void delete(Long id){
        find(id);
        clienteRepository.deleteById(id);
    }
}
