package br.com.avaliacao.tecnica;

import br.com.avaliacao.tecnica.domain.*;
import br.com.avaliacao.tecnica.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.text.SimpleDateFormat;
import java.util.Arrays;

@SpringBootApplication
public class AvaliacaoApplication implements CommandLineRunner {

    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private RamoAtividadeRepository ramoAtividadeRepository;
    @Autowired
    private ConcorrenteRepository concorrenteRepository;
    @Autowired
    private PropostaRepository propostaRepository;

    public static void main(String[] args) {
        SpringApplication.run(AvaliacaoApplication.class, args);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods("GET", "POST", "DELETE", "PUT", "OPTIONS").allowedHeaders("*").allowedOrigins("*");
            }
        };
    }

    @Override
    public void run(String... args) throws Exception {


        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");

        Proposta proposta1 = new Proposta(0.2, 0.5, sdf.parse("29/01/2018 10:32"));
        Proposta proposta2 = new Proposta(1.0, 0.3, sdf.parse("29/01/2018 10:32"));

        RamoAtividade ramoAtividade1 = new RamoAtividade("Farmácias", 3.55, 1.99);

        RamoAtividade ramoAtividade2 = new RamoAtividade("Ramo Atividade 2", 3.55, 1.99);


        Concorrente concorrente1 = new Concorrente("Concorrente 1", 3.3, 2.1);
        Concorrente concorrente2 = new Concorrente("Concorrente 2", 4.3, 2.3);
        Concorrente concorrente3 = new Concorrente("Concorrente 3", 3.3, 2.6);
        Concorrente concorrente4 = new Concorrente("Concorrente 4", 5.3, 2.9);
        Concorrente concorrente5 = new Concorrente("Concorrente 5", 3.3, 2.2);
        Concorrente concorrente6 = new Concorrente("Concorrente 6", 3.9, 2.0);

        Cliente cli1 = new Cliente(concorrente1, "061116699", "teste1@gmail.com", ramoAtividade1, proposta1);
        cli1.getTelefones().addAll(Arrays.asList("27363323", "489952365"));
        cli1.getProposta().setAceita(true);


        Cliente cli2 = new Cliente(concorrente1, "061116699", "teste2@gmail.com", ramoAtividade2, proposta2);
        cli2.getTelefones().addAll(Arrays.asList("123666556", "12335666"));
        cli2.getProposta().setAceita(false);

        ramoAtividade1.getClientes().add(cli1);
        ramoAtividade2.getClientes().add(cli2);



        propostaRepository.saveAll(Arrays.asList(proposta1, proposta2));
        ramoAtividadeRepository.saveAll(Arrays.asList(ramoAtividade1, ramoAtividade2));
        concorrenteRepository.saveAll(Arrays.asList(concorrente1, concorrente2, concorrente3, concorrente4, concorrente5, concorrente6));
        clienteRepository.saveAll(Arrays.asList(cli1, cli2));
    }
}



